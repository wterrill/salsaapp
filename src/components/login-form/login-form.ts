import { Component, Input, Output, EventEmitter } from '@angular/core';

/**
 * Generated class for the LoginFormComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'login-form',
  templateUrl: 'login-form.html'
})
export class LoginFormComponent {

  @Input('username') username;
  @Input('password') password;
  
  @Output() somethingHappened = new EventEmitter;

  text: string;

  constructor() {
    console.log('Hello LoginFormComponent Component');
    this.text = "default Text"
  }

  ngAfterViewInit(){

  }

}
