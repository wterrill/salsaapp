import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EmailComposer } from '@ionic-native/email-composer';
import { AuthenticationProvider } from "../../providers/authentication/authentication";

/**
 * Generated class for the FeedbackPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-feedback',
  templateUrl: 'feedback.html',
})
export class FeedbackPage {

  constructor(
    private navCtrl: NavController,
    private navParams: NavParams,
    private emailComposer: EmailComposer,
    private auth: AuthenticationProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FeedbackPage');
  }


sendIt(){
  // this.emailComposer.isAvailable().then((available: boolean) =>{
  //   if(available) {
  //     alert("beer")
  //   }
  //  });


  let email = {
    to: 'salsaconnectchicago@gmail.com',
    cc: 'william.terrill@gmail.com',
    //bcc: ['john@doe.com', 'jane@doe.com'],
    attachments: [
      // 'file://img/logo.png',
      // 'res://icon.png',
      // 'base64:icon.png//iVBORw0KGgoAAAANSUhEUg...',
      // 'file://README.pdf'
    ],
    subject: 'Feedback from Salsa Connect user: '+this.auth.currentUser.email,
    body: '',
    isHtml: false
  };
  
  // Send a text message using default options
  this.emailComposer.open(email);

}


}



