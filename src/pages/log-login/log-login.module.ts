import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogLoginPage } from './log-login';

@NgModule({
  declarations: [
    LogLoginPage,
  ],
  imports: [
    IonicPageModule.forChild(LogLoginPage),
  ],
})
export class LogLoginPageModule {}
