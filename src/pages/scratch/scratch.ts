import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Platform } from "ionic-angular";

/**
 * Generated class for the ScratchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-scratch',
  templateUrl: 'scratch.html',
})
export class ScratchPage {

  private platform_type = null;
  private version = null;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public platform: Platform
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ScratchPage');
      ////////////this.platform_type = this.platform.platforms()
      ////////////this.version = this.platform.versions()

      this.platform_type = this.platform.platforms();
      alert(this.platform_type)
      alert(JSON.stringify(this.platform.versions()))
  }

}
