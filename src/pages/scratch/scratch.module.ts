import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScratchPage } from './scratch';

@NgModule({
  declarations: [
    ScratchPage,
  ],
  imports: [
    IonicPageModule.forChild(ScratchPage),
  ],
})
export class ScratchPageModule {}
