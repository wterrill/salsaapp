import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { BrowserTab } from '@ionic-native/browser-tab';


/**
 * Generated class for the NewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {
  private html = null;
  options: InAppBrowserOptions = {
    location: 'no',//Or 'yes' 
    hidden: 'no', //Or  'yes'
    clearcache: 'yes',
    clearsessioncache: 'yes',
    zoom: 'yes',//Android only ,shows browser zoom controls 
    hardwareback: 'yes',
    mediaPlaybackRequiresUserAction: 'no',
    shouldPauseOnSuspend: 'no', //Android only 
    closebuttoncaption: 'Close', //iOS only
    disallowoverscroll: 'no', //iOS only 
    toolbar: 'no', //iOS only 
    enableViewportScale: 'no', //iOS only 
    allowInlineMediaPlayback: 'no',//iOS only 
    presentationstyle: 'formsheet',//iOS only 
    //fullscreen: 'yes',//Windows only    
    closebuttoncolor: 'purple' ,
    
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private iab: InAppBrowser,
    private browserTab: BrowserTab
  ) {
    this.html = `
    <div>
    <p>This is a line in a div</p>
    <p>This is another line in a div</p>
    </div>
    <div>
    <p>This is a line in another div</p>
    this is a non p line
    </div>
    <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Codelco_logo.svg/120px-Codelco_logo.svg.png">`

    // const browser = this.iab.create('http://inglesconelgringo.com/');

    // //browser.executeScript(...);

    // //browser.insertCSS(...);
    // browser.on('loadstop').subscribe(event => {
    //   browser.insertCSS({ code: "body{color: red;" });
    // });

    // browser.close();


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewsPage');
  }

  public openWithSystemBrowser(url: string) {
    let target = "_system";
    this.iab.create(url, target, this.options);
  }
  public openWithInAppBrowser(url: string) {
    let target = "_blank";
    this.iab.create(url, target, this.options);
  }
  public openWithCordovaBrowser(url: string) {
    let target = "_self";
    this.iab.create(url, target, this.options);
  }
  public openWithBrowserTab(url: string) {
  this.browserTab.isAvailable()
    .then(isAvailable => {
      if (isAvailable) {
        this.browserTab.openUrl(url);
      } else {
        // open URL with InAppBrowser instead or SafariViewController
      }
    });
  }



  


  // browserTab.isAvailable()
  //   .then(isAvailable => {
  //     if (isAvailable) {
  //       browserTab.openUrl('https://ionic.io');
  //     } else {
  //       // open URL with InAppBrowser instead or SafariViewController
  //     }
  //   });



}
