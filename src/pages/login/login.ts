import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import * as firebase from "firebase";
import { AlertController } from "ionic-angular";
import { AuthenticationProvider } from "../../providers/authentication/authentication";
import { Storage } from "@ionic/storage";
import * as moment from "moment";
import { AppVersion } from "@ionic-native/app-version";
import { Platform } from "ionic-angular";
import { ConfigurationProvider } from "../../providers/configuration/configuration";

//https://firebase.google.com/docs/auth/web/start?authuser=0

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  private todo: FormGroup;
  private email_var: string = null;
  private password_var: string = null;
  private provider = new firebase.auth.GoogleAuthProvider();
  private debug: boolean = false;
  private localuser = null;
  private pointerAnim = true;
  private rememberMe = false;
  private superUser = false;
  private uid = null;
  private email = null;
  private platform_type = null;
  private version = null;
  // private pic1 = "https://placeimg.com/640/480/nature";
  // private pic2 = "https://placeimg.com/640/480/arch";
  // private pic3 = "https://placeimg.com/640/480/";

  // private pic1 = "assets/imgs/IMG-0378.png";
  // private pic2 = "assets/imgs/IMG_0404.png";
  // private pic3 = "assets/imgs/WillMitsi.png";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    private auth: AuthenticationProvider,
    public storage: Storage,
    private appVersion: AppVersion,
    private platform: Platform,
    private config: ConfigurationProvider
  ) {
    this.todo = this.formBuilder.group({
      email_form: ["", Validators.required],
      password_form: ["", Validators.required]
    });

    this.storage.get("credentials").then(val => {
      //alert(val)
      if (val) {
        this.email_var = val[0];
        this.password_var = val[1];
        this.rememberMe = true;
      }
    });

    //this.provider.addScope("https://www.googleapis.com/auth/contacts.readonly"); //get additional scope from google
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad LoginPage");
    if (this.platform.is("cordova")) {
      // This will only print when running on mobile
      this.platform_type = this.platform.platforms()
      this.version = JSON.stringify(this.platform.versions())
    }
  }

  updateHistoryDatabase() {
    //log individual history
    var uid = this.uid;
    firebase
      .database()
      .ref("users/" + uid + "/login_history")
      .push(
        moment().toString()
      );

    //log app version
    this.appVersion.getVersionNumber().then(versionNumber => {
      firebase
        .database()
        .ref("users/" + uid + "/app_version")
        .update(
          versionNumber
        );
    });

    //log email ->  I'll probably get rid of this in the future TODO /////  this is a stop gap to capture everyone's email
    // firebase
    //   .database()
    //   .ref("users/" + uid + "/email")
    //   .update(
    //     this.email
    //   );
    
    //log general visitation with email and dateTime
    if(this.email !="william.terrill@gmail.com" && this.email!="slade.brockett@gmail.com")
    {
      firebase
      .database()
      .ref("visitors/")
      .push(
        this.email + ":" + moment().toString()
      );
    }

    // log platform and email. 
    firebase
      .database()
      .ref("users/" + this.uid)
      .update({
        platform_type: this.platform_type,
        platform_version: this.version,
        email: this.email
      });


  }


  signInUser(email, password) {
    //let persistanceFirebase:any = ;
    //this.signOut(); // make sure the user is signed out so that we get new data.
    var that = this;
    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(result => {
        console.log(result);
        that.onAuthChange(email, password);
      })
      .catch(function (error) {
        // Handle Errors here.
        if (that.debug) {
          alert("There was an error");
        }
        var errorCode = error.code;
        var errorMessage = error.message;
        if (that.debug) {
          alert("signInUserError:" + errorCode + ":-:" + errorMessage);
        }
        that.presentError(errorCode, errorMessage);
      });
  }

  onAuthChange(email, password) {
    //var that = this;
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.localuser = user;
        if (!user.emailVerified) {
          //user exists, but email not verified
          console.log("Email is not verified");
          this.presentResend(user);
        } else {
          this.getUserData(user.uid);
          this.auth.currentUser = user;
          //user exist and email verified.
          // User is signed in.
          var displayName = user.displayName;
          var email = user.email;
          var emailVerified = user.emailVerified;
          var photoURL = user.photoURL;
          var phoneNumber = user.phoneNumber;
          var isAnonymous = user.isAnonymous;
          console.log(user)
          this.uid = user.uid;
          this.email = user.email;
          var providerData = user.providerData;
          console.log(
            "onStateChanged::" +
            displayName +
            ":" +
            this.email +
            ":" +
            emailVerified +
            ":" +
            photoURL +
            ":" +
            isAnonymous +
            ":" +
            this.uid +
            ":" +
            providerData +
            ":" +
            phoneNumber
          );

          //this section controls where the app will go after login
          //first login -> create profile and slideshow
          //each subsequent login -> show slide show if not asked to never show it again
          //after that, landing page.

          //This is the first run-through,
          //so, they'll have to watch the slide show:

          if (!user.photoURL) {
            if (this.rememberMe) {
              this.storage.set("credentials", [email, password]); //Save user credientials
            } else {
              this.storage.remove("credentials");
            }

            this.navCtrl.push("CreateProfilePage");
            this.storage.set("show_slideshow", true);
          } else {
            console.log("*****************************************************************")
            this.getAppData();
            console.log("***-***-***-***-***-***-*****************************************")
            this.storage.get("show_slideshow").then(val => {
              //if they haven't told "never again" to the slideshow
              //make them watch it
              if (this.rememberMe) {
                this.storage.set("credentials", [email, password]); //Save user credientials
              } else {
                this.storage.remove("credentials");
              }
              if ((val == true || val == undefined) && !this.superUser) {
                this.navCtrl.setRoot("SlideshowPage");
              } else if (!this.superUser) {
                this.navCtrl.setRoot("LandingPage");
              } else {
                this.navCtrl.push("HomePage");
              }
            });
          }
          this.updateHistoryDatabase();
        } //end else for if(user);
      } else {
        //user does not exist.
        if (this.debug) {
          alert("user does not exist");
        }
      }
    });
  }
  /*
    setPersistence(email,password) {
      var that = this;
      firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL)
      .then(function() {
        // Existing and future Auth states are now persisted in the current
        // session only. Closing the window would clear any existing state even
        // if a user forgets to sign out.
        // ...
        // New sign-in will be persisted with session persistence.
        alert("setting persistance")
        firebase.auth().signInWithEmailAndPassword(email, password)
        .then(result => {
          console.log(result);
          that.onAuthChange(email, password);
        })
        .catch(function(error) {
          // Handle Errors here.
          if (that.debug) {
            alert("There was an error");
          }
          var errorCode = error.code;
          var errorMessage = error.message;
          if (that.debug) {
            alert("signInUserError:" + errorCode + ":-:" + errorMessage);
          }
          that.presentError(errorCode, errorMessage);
        });
      })
      .catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        alert(errorCode + " " + errorMessage);
      });
    }
    */


  submitted() {
    this.email_var = this.email_var.replace(/ /g, "")
    if (this.debug) {
      alert(this.email_var + this.password_var);
    }
    //this is for the case where an email or password is not entered.
    if (this.email_var && this.password_var) {
      this.signInUser(this.email_var, this.password_var);
    } else {
      this.presentError(
        "Complete Log in",
        "Please enter both a username and password"
      );
    }
  }

  signInGooglePopup() {
    firebase
      .auth()
      .signInWithPopup(this.provider)
      .then(function (result) {
        // This gives you a Google Access Token. You can use it to access the Google API.
        var token = result.credential.accessToken;
        console.log(token);
        // The signed-in user info.
        var user = result.user;
        console.log(user);
        // ...
      })
      .catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;

        // The email of the user's account used.
        var email = error.email;

        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;

        console.log(errorCode);
        console.log(email);
        console.log(errorMessage);
        console.log(credential);
        // ...
      });
  }

  signInGoogleRedirect() {
    firebase.auth().signInWithRedirect(this.provider);

    firebase
      .auth()
      .getRedirectResult()
      .then(function (result) {
        if (result.credential) {
          // This gives you a Google Access Token. You can use it to access the Google API.
          var token = result.credential.accessToken;
          console.log(token);
        }
        // The signed-in user info.
        var user = result.user;
        console.log(user);
      })
      .catch(function (error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        console.log(errorCode);
        console.log(errorMessage);
        console.log(email);
        console.log(credential);

        // ...
      });
  }

  signOut() {
    var that = this;
    firebase
      .auth()
      .signOut()
      .then(function () {
        // Sign-out successful.
        that.localuser = null;
      })
      .catch(function (error) {
        // An error happened.
      });
  }

  register() {
    this.navCtrl.push("SignUpPage");
  }

  forgotPassword() {
    this.navCtrl.push("ForgotPasswordPage");
  }

  presentResend(user) {
    let alert = this.alertCtrl.create({
      title: "Email needs to be verified",
      message: `This email account is not yet verified.<br> 
                Would you like us to send you another verification email? `,
      buttons: [
        {
          text: "Yes, resend verification email.",
          role: "cancel",
          handler: () => {
            this.navCtrl.push("LoginPage");
            var that = this;
            user.sendEmailVerification().then(function () {
              that.presentInfo();
              that.signOut();
            });
          }
        },
        {
          text: "No.",
          role: "cancel"
        }
      ]
    });
    alert.present();
  }

  presentInfo() {
    let alert = this.alertCtrl.create({
      title: "Confirmation Email",
      message: `A confirmation Email has been sent to your email address.<br> 
                Please continue the registration process by visiting your
                email account, and confirming the address. <br> After the email address is confirmed, sign into the app. `,
      buttons: [
        {
          text: "Ok",
          role: "cancel"
        }
      ]
    });
    alert.present();
  }

  presentError(errorCode, errorMessage) {
    let alert = this.alertCtrl.create({
      title: "Error Code: " + errorCode,
      message: errorMessage,
      buttons: [
        {
          text: "Try Again",
          role: "cancel"
        }
      ]
    });
    alert.present();
  }

  gotoHome() {
    this.superUser = true;
    this.signInUser("william.terrill@gmail.com", "1Fraggle");
  }

  stopAnim() {
    this.pointerAnim = false;
  }

  clear() {
    if (!this.rememberMe) {
      this.email_var = null;
      this.password_var = null;
    }
  }

  async getUserData(uid) {
    var that = this;
   await firebase
      .database()
      .ref("/users/" + uid)
      .once("value")
      .then(snapshot => {
        var result = snapshot.val();
        that.auth.currentUserData = result;
      }); //end firebase.database
  }

  async getAppData(){
    //var that = this;
   let snapshot = await firebase
      .database()
      .ref("/AppConfig/")
      .once("value")
      try{
        console.log("** ** ** ** ** ** ** ** ** **")
        var result = await snapshot.val();
        console.log(result)
        this.config.news = result.news
        this.config.routineServer = result.routineServer
        this.config.facebook = result.facebook
        console.log("** ** ** ** ** ** ** ** ** **")
        console.log(this.config.news)
        console.log(this.config.facebook)
      }
      catch(e) {
        console.log('Error!', e);
      }
      // .then(snapshot => {
      //   var result = snapshot.val();
      //   that.config.news = result.news
      //   that.config.routineServer = result.routineServer
      //   that.config.facebook = result.facebook
      // }); //end firebase.database
  }
}
