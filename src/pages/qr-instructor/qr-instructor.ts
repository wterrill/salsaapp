import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular'; //NavController, NavParams 
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { AlertController } from 'ionic-angular';
import * as moment from "moment";

/**
 * Generated class for the QrInstructorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-qr-instructor',
  templateUrl: 'qr-instructor.html',
})
export class QrInstructorPage {
  private scannedCode:any = null;
  private studentPhoto = null;
  private displayName = null;
  private email = null;
  private uid = null;
  private amount = null;
  private date = null;
  private paidUntil = null;
  private cost = null;
  private result = null;

    // "username": this.username.displayName,
    // "uid":this.username.uid,
    // "photo":this.username.photoURL,
    // "email":this.username.email, 
    // "amount": this.amount, 
    // "date": this.date, 
    // "paidUntil": this.period, 
    // "cost": this.cost


  constructor(
    // private navCtrl: NavController, 
    // private navParams: NavParams,
    private barcodeScanner: BarcodeScanner,
    private alertCtrl: AlertController
    ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad QrInstructorPage');
  }

  scanCode(){
    this.barcodeScanner.scan().then(barcodeData => {
      alert(JSON.stringify(barcodeData));
      this.scannedCode = barcodeData.text;
      this.displayInfo();
    }).catch(err => {
      //alert('Error' + err);
  });
  }

  displayInfo(){
    this.result = JSON.parse(this.scannedCode);
    this.studentPhoto = this.result.photo;
    this.displayName = this.result.displayName;
    this.email = this.result.email;
    this.uid = this.result.uid;
    this.amount = this.result.amount;
    this.date = this.result.date;
    this.paidUntil = this.result.paidUntil;
    this.cost = this.result.cost;
    //this.presentConfirm(this.result.displayName, this.result.amount, this.result.date)  /////////////////////////////////////////////////

    // "username": this.username.displayName,
    // "uid":this.username.uid,
    // "photo":this.username.photoURL,
    // "email":this.username.email, 
    // "amount": this.amount, 
    // "date": this.date, 
    // "paidUntil": this.period, 
    // "cost": this.cost


  }

  presentConfirm(name, amount, date) {
    let alert = this.alertCtrl.create({
      title: 'Please confirm purchase',
      message: 'Did - User: ' + name +"<br> pay: $" + amount + "<br> on " + moment(date).format('YYYY-MM-DD') + "?",
      buttons: [
        {
          text: 'Nope',
          role: 'cancel',
          handler: () => {
            console.log("cancelled");
          }
        },
        {
          text: 'YES - Scan another',
          handler: () => {
              this.scanCode()
              //go to database
          }
        },
        {
          text: 'YES - Stop scanning',
          handler: () => {
              console.log("stop scanning");
              //go to databasse
          }
        }
      ]
    });
    alert.present();
  }

}
