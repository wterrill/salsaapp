import { Component } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular"; //NavParams
import * as firebase from "firebase";
import * as Fuse from "fuse.js";
import _ from "lodash";
import { AlertController } from "ionic-angular";
// import moment from "moment";
import { PopoverController } from "ionic-angular";
import { PopoverComponent } from "../../components/popover/popover";
import { AuthenticationProvider } from "../../providers/authentication/authentication";
//import { keyframes } from "@angular/animations";
import { ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: "page-accounts",
  templateUrl: "accounts.html"
})
export class AccountsPage {
  //private results: any = null;
  private userArray = [];
  private userArray2 = [];
  private debug: Boolean = false;

  //private beer = "rotate()"

  // variables for search
  private filteredSearchItems = null;
  private searchItems = null;
  private searching = null;
  private searchInput = null;

  //variables for display
  private gridDisplay = null;
  private myBgColor = null;
  private selectedColor = "selected";
  private unselectedColor = "unselected";

  //variables for menu actions
  private actionArray = [];
  private admin = false;
  private classes = [];
  private availClasses = null;

  constructor(
    public navCtrl: NavController,
    //private navParams: NavParams,
    private alertCtrl: AlertController,
    private popoverCtrl: PopoverController,
    private auth: AuthenticationProvider,
    public modalCtrl: ModalController
  ) {
    console.log(this.userArray);
    if (this.auth.currentUserData.permissions === "administrator") {
      this.admin = true;
    }
    this.loadAvailClasses()
  } // end of constructor

  ionViewDidLoad() {
    //alert("ionViewDidLoad AccountsPage");
    firebase
      .database()
      .ref("/users/")
      //.orderByChild("birthday.month")
      .orderByChild("displayName")
      .once("value")
      .then(snapshot => {
        var result = snapshot.val();
        console.log("result");
        console.log(result);
        console.log("snapshot");
        console.log(snapshot);
        for (var key in result) {
          if (result.hasOwnProperty(key)) {
            var val = result[key];
            console.log("key", key);
            console.log("val");
            console.log(val);
            console.log(val.profile_pic);
            console.log("___");
            val.uid = key;
            if (val.profile_pic != "undefined") {
              console.log(val);
              this.userArray.push(val);
            }
          } //end of ir(result.hasOwnProperty(key))
        } //end var key in result

        console.log(this.userArray);
        this.userArray2 = Object.keys(result).map(function (key) {
          return [Number(key), result[key]];
        });
        console.log("userArray:", this.userArray)
        console.log("userArray2:",this.userArray2);
        this.setupDisplay(this.userArray);
        this.filteredSearchItems = this.userArray;
      }); //end firebase.database
  }

  // piccies() {
  //   console.log("piccies");
  //   console.log(this.results[0]);
  // }

  selected(ev, user, i) {
    ev.stopPropagation(); //this stops firing any other buttons on the ion-card
    if (this.myBgColor[i] == this.unselectedColor) {
      this.myBgColor[i] = this.selectedColor;
      this.actionArray.push(user);
    } else {
      this.myBgColor[i] = this.unselectedColor;
      var index = this.actionArray
        .map(function (e) {
          return e.uid;
        })
        .indexOf(user.uid); //this.actionArray.indexOf(user.uid);
      if (index > -1) {
        this.actionArray.splice(index, 1);
      }
    }
    if (this.debug) {
      alert(this.actionArray);
    }
  }

  selectAllStudents(){
    this.actionArray = this.filteredSearchItems;
    for(var i = 0; i<this.myBgColor.length; i++){
        this.myBgColor[i]=this.selectedColor
    }
  }

  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();
    if (this.debug) {
      console.log("searchItems", this.searchItems);
      console.log("this.userArray", this.userArray);
    }

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (this.debug) {
      console.log("ev", ev);
      console.log("val", val);
      console.log(ev.data);
    }
    if (ev.data == null) {
      // if this is null, then the user hit 'backspace' to kill everything
      //this.updatelist;   // so reset the filters to show the restaurants again.
    }

    if(val == null){
      this.reset();
    }
    
    if (val!=null && val.length < 1 && this.searching == true) {
      //this case is invoked if we were searching, but we deleted to start again.
      this.searching = false; //this.searching tells the html that we are actively searching for stuff, so it changes what is displayed.
    }

    if (val!=null && val.length > 2) {
      //don't start search unless we have at least three characters.
      this.searching = true; //this shows that we're searching and changes what is seen in the html
      this.filteredSearchItems = this.fuseSearch(val); //Search it!!!
      this.gridDisplay = false; //Clear it!!!
      this.setupDisplay(this.filteredSearchItems); //Display it!!!
    }
  }

  initializeItems() {
    if (this.debug) {
      console.log("initializeItems");
    }
    this.searchItems = this.userArray;
    if (this.debug) {
      console.log("initializeItems searchItems", this.searchItems);
    }
    this.filteredSearchItems = this.userArray;
    if (this.debug) {
      console.log(
        "initial filteredSearchItems searchItems",
        this.filteredSearchItems
      );
    }
  }

  reset(){
    this.gridDisplay = null;
    this.userArray = [];
    this.searchInput = "";
    this.actionArray = [];
    this.filteredSearchItems= [];
    this.initializeItems();
    this.ionViewDidLoad();
  }

  fuseSearch(searchTerm) {
    //this does the actual fuzzy search.
    var options = {
      shouldSort: true,
      matchAllTokens: true,
      tokenize: true,
      includeScore: true,
      includeMatches: true,
      threshold: 0.1,
      //location: 0,  these values aren't needed if you match all tokens.
      //distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      findAllMatches: true,
      keys: ["firstName", "lastName", "currentClass.name", "displayName"]
    };

    if (this.debug) {
      console.log("fuzzywuzzy", searchTerm);
    }
    var fuse = new Fuse(this.userArray, options); //this.result is the full-on data.
    var searchResults = fuse.search(searchTerm);
    if (this.debug) {
      console.log("fuzzywuzzy-results", searchResults);
    }

    //Massaging the data before returning it. I'm taking the meta-data provided by
    //Fuse, and sticking it back into the filtered results.
    //the lodash map function below steps through each search result...
    var arr = _.map(searchResults, function (el) {
      el.item.matches = el.matches; // the matches matrix shows what actually matched during the search.  We're storing it inside of filteredSearchItems
      var matchedFoodArray = _.map(el.matches, function (le) {
        if (le.key == "menu_items.name") {
          return el.item.menu_items[le.arrayIndex];
        } //Here's we're using the array index for the matching items to look up the actual menu items.
      });
      if (matchedFoodArray[0] != undefined) {
        el.item.matching_food = matchedFoodArray; //the matching_food array is the one that will be displayed on the search screen results.
      }
      return el.item;
    });

    if (this.debug) {
      console.log("final array", arr);
    }
    return arr;
  } // end of fuse search

  setupDisplay(array) {
    //setupDisplay takes an array of users and creates an array of grids and background colors
    //; //counter to iterate over the rows in the grid
    let userGrid = [];
    let color = [];
    for (let i = 0; i < array.length; i++) {
        //check user exists
        if (array[i].currentClass.name !== "Tester-only"){
          //console.log(array[i].currentClass)
          userGrid.push(array[i]); //insert user
          color.push(this.unselectedColor); 
        } else if(this.admin){
          userGrid.push(array[i]); //insert user
          color.push(this.unselectedColor); 
        }
    }
    console.log(userGrid);
    this.gridDisplay = userGrid;
    this.myBgColor = color;
  }



  // presentStudentData(user) {
  //   alert(user.rotate);
  //   let beer = "rotate90"
  //   let alertx = this.alertCtrl.create({
  //     title: "Student Information",
  //     message:
  //       `<img  src= ` +
  //       user.profile_pic +
  //       ` />
  //                 <p>Name: &nbsp;` +
  //       user.displayName +
  //       `</p>   
  //                 <p>birthday:&nbsp;` +
  //       moment(user.birthday)
  //         .add(-1, "M")
  //         .format("MMMM DD") +
  //       `</p> ` + //month is subtracted because momentjs indexes month by zero.
  //       `<p>Class: ` +
  //       user.currentClass.shortName +
  //       `</p>
  //                 <p>Bio: ` +
  //       user.bio +
  //       `</p>`,
  //     buttons: [
  //       {
  //         text: "Ok",
  //         role: "cancel",
  //         handler: () => {
  //           console.log("cancelled");
  //         }
  //       }
  //     ]
  //   });
  //   alertx.present();
  // }

  showInfo(event, user) {
    //this.presentStudentData(user);
    this.presentContactModal(user);
    event.stopPropagation();
  }

  presentContactModal(user) {
    const contactModal = this.modalCtrl.create("UserInfoPage",{userID: user});
    contactModal.present();
  }

  presentPopover(myEvent) {
    let listData = [
      { title: "Add to Class", submenu: this.classes, id: 1 },
      // {
      //   title: "Promote to next level",
      //   submenu: [{ title: "submenu1" }, { title: "submenu2" }],
      //   id: 2
      // },
      { title: "Enable Routines", submenu: [], id: 3 },
      { title: "Select All Students", submenu: [], id: 4 },
      { title: "Make Administrator", submenu: [], id: 5 },
      { title: "Create New Class", submenu: [], id: 6 }
    ];
    console.log("accounts listData", listData);
    let popover = this.popoverCtrl.create(PopoverComponent, {
      header: "Instructor Options",
      listData
    });
    popover.present({
      ev: myEvent
    });

    popover.onDidDismiss(menuSelect => {
      if (menuSelect == null) {
        return;
      }
      if (this.actionArray.length === 0 && menuSelect.title !== "Create New Class"&& menuSelect.title !== "Select All Students") {
        let messagetxt =
          "Please select the students to update by clicking on their images before selecting an action to perform.";
        this.presentError(messagetxt);
      } else {
        this.handleSelection(menuSelect, this.actionArray);
      }
    });
  }

  presentError(messagetxt) {
    let alert = this.alertCtrl.create({
      title: "Error",
      message: messagetxt,
      buttons: [
        {
          text: "Ok",
          role: "cancel",
          handler: () => {
            console.log("cancelled");
          }
        }
      ]
    });
    alert.present();
  }

  makeAdministrator(target) {
    this.updateDatabase(target, { permissions: "administrator" }, "users/")

  }

  updateDatabase(who, what, where) {
    for (var i = 0; i < who.length; i++) {
      firebase
        .database()
        .ref(where + who[i].uid)
        .update(what).then(x => { 
          if (x!=undefined){
            alert("updateDatabase returns:"+x) 
          } else {
            //this.reset();
          }
        });
      //this.presentToast(this.target[i]);
    }
  }


  handleSelection(task, target) {
    if(this.debug){alert(JSON.stringify("task:" + task + " target:" + target));}
    //this is where the modal page will be called
    if (task.title === "Enable Routines") {
      this.actionArray = [];
      for (var i = 0; i < this.myBgColor.length; i++) {
          //this resets the selection on the page for when you return.
          this.myBgColor[i] = this.unselectedColor;
        
      }
      this.navCtrl.push("RestrictVideoPage", { target });
    }
    else if (task.title === "Make Administrator") {
      this.makeAdministrator(target);

    }
    else if (task.title === "Create New Class") {
      this.navCtrl.push("CreateNewClassPage")

    }
    else if (task.title === "Select All Students"){
      this.selectAllStudents();
    }
    else if (task.id.includes("1-")) {
      var temp = task.id.split("-")
      var index = temp[1]
      var jsonKeys = Object.keys(this.availClasses)
      var key = jsonKeys[parseInt(index)]

      var selectedClass = this.availClasses[key]
      var selectedClassStructure = selectedClass.structure;
      if (this.debug) {
        alert(key)
        alert(JSON.stringify(selectedClass.structure));
      }
      this.updateDatabase(target, { currentClass: selectedClassStructure }, "users/")
      this.reset();
    }
    else {
      let messagetxt = "This option hasn't been turned on yet... sorry";
      this.presentError(messagetxt);
    }
  }

  loadAvailClasses() {
    var that = this;
    firebase.database().ref('availClasses/').once('value').then(snapshot => {
      that.availClasses = snapshot.val()
      if (snapshot.val() !== null) {
        Object.keys(that.availClasses).forEach(function (key, idx) {
          that.classes.push({ title: that.availClasses[key].structure.shortNameFull, id: "1-" + idx })
        });
      }
    });
  }
} // end of class
