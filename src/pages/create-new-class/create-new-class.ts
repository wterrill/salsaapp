import { Component } from '@angular/core';
import { IonicPage} from 'ionic-angular'; //NavController, NavParams 
import * as firebase from "firebase";
import { AlertController } from 'ionic-angular';


/**
 * Generated class for the CreateNewClassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-create-new-class',
  templateUrl: 'create-new-class.html',
})
export class CreateNewClassPage {

  private level = null;
  private day = null;
  private month = null;
  private year = null;
  private comment = null;

  //private classes


  constructor(
    //private navCtrl: NavController,
    //private navParams: NavParams,
    private alertCtrl: AlertController
  ) {
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad CreateNewClassPage');
  }

  makeNewClass() {
    let dayNum = -1;
    if(this.day == "Monday"){
      dayNum = 1
    } else
    if(this.day == "Tuesday"){
      dayNum = 2
    } else
    if(this.day == "Wednesday"){
      dayNum = 3
    } else
    if(this.day == "Thursday"){
      dayNum = 4
    } else
    if(this.day == "Friday"){
      dayNum = 5
    }
    if(this.day == "Saturday"){
      dayNum = 6
    }
    if(this.day == "Sunday"){
      dayNum = 0
    }
    let structure = {
      level: this.level,
      day: this.day,
      dayNum: dayNum,
      month: this.month,
      year: this.year,
      comment: this.comment,
      name: this.level + "-" + this.day + "-" + this.month + "-" + this.year,
      shortName: this.level + "-" + this.day,
      shortNameFull: this.level.substring(0, 3) + "-" + this.day.substring(0,3)  + "-" + this.month.substring(0,3) + "-" + this.year.substring(2,4)
    }
    if (this.level && this.day && this.month && this.year) {
      firebase
        .database()
        .ref("availClasses/")
        .push({
          structure
        }).then(x=>{
          //alert("new class return : "+x)
          this.presentSuccess();
        });
    } else {
      this.presentError();
    }




    //firebase.database().ref('/userProfile').push(userProfile)
  }//end makeNewClass


  presentError() {
    let alert = this.alertCtrl.create({
      title: 'Error in Class Name',
      subTitle: 'The level, day, month and year are needed to initialize a new class name',
      buttons: ['Dismiss']
    });
    alert.present();
  }

  presentSuccess() {
    let alert = this.alertCtrl.create({
      title: 'Created New Class ',
      subTitle: 'The new class was successfully created',
      buttons: ['Dismiss']
    });
    alert.present();
  }

  // loadAvailClasses() {
  //   //var userId = firebase.auth().currentUser.uid;
  //   firebase.database().ref('availClasses/').once('value').then(snapshot => {
  //     this.classes = []
  //     var that = this
  //     //this.classes = JSON.stringify(snapshot.val())
  //     var data = snapshot.val()
  //     Object.keys(data).forEach(function (key, idx) {
  //       that.classes.push(data[key].structure.name)
  //     });
  //   });
  // }


}
