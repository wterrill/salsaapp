import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateNewClassPage } from './create-new-class';

@NgModule({
  declarations: [
    CreateNewClassPage,
  ],
  imports: [
    IonicPageModule.forChild(CreateNewClassPage),
  ],
})
export class CreateNewClassPageModule {}
