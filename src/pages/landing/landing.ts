import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import * as firebase from "firebase";
import { AppVersion } from "@ionic-native/app-version";
import { AlertController } from "ionic-angular";
import { ConfigurationProvider } from "../../providers/configuration/configuration";
import { BrowserTab } from '@ionic-native/browser-tab';
//import { version } from "moment";

// Splits, normalizes into 3 numbers, zeroes invalid values
// const filler = [0, 0, 0];

@IonicPage()
@Component({
  selector: "page-landing",
  templateUrl: "landing.html"
})
export class LandingPage {
  private debug: boolean = false;
  private disable_button = false;
  private news = null;
  private facebook = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public appVersion: AppVersion,
    private alertCtrl: AlertController,
    private config: ConfigurationProvider,
    private browserTab: BrowserTab,
  ) {
    this.news = this.config.news
    this.facebook = this.config.facebook
    console.log("neeeeeews:", this.news)
    this.versionCheck();
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad LandingPage");
  }

  gotoVideoPicker() {
    this.navCtrl.push("VideoPickerPage");
  }

  gotoPay() {
    this.navCtrl.push("QrStudentPage");
  }

  gotoScan() {
    this.navCtrl.push("QrInstructorPage");
  }

  gotoAccount() {
    this.navCtrl.push("UpdateProfilePage");
  }

  gotoAccounts(){
    this.navCtrl.push("AccountsEntryPage");
  }

  signOut() {
    var that = this;
    firebase
      .auth()
      .signOut()
      .then(function() {
        // Sign-out successful.
        //that.localuser = null;
        that.navCtrl.setRoot("LoginPage");
      })
      .catch(function(error) {
        // An error happened.
      });
  }

  async showVersion() {
    let minVersion = null;
    let latestVersion = null;
    var videoArray = firebase.database().ref("AppInfo");
    videoArray.on("value", snapshot => {
      if (this.debug) {
        alert("snapshot: " + JSON.stringify(snapshot));
        alert("minVersion = " + snapshot.val().minVersion);
      }
      minVersion = snapshot.val().minVersion;
      latestVersion = snapshot.val().latestVersion;
    });

    //let name = await this.appVersion.getAppName(); //.then(x =>{ name = x
    //let packageName = await this.appVersion.getPackageName(); //.then(x =>{ packageName = x
    //let versionCode = await this.appVersion.getVersionCode(); //.then(x =>{ versionCode = x
    let versionNumber = await this.appVersion.getVersionNumber(); //.then(x =>{ versionNumber = x
    this.presentConfirm(
      versionNumber,
      minVersion,
      latestVersion
    );
  }

  async versionCheck() {
    let minVersion = null;
    let latestVersion = null;
    var videoArray = firebase.database().ref("AppInfo");
    videoArray.on("value", snapshot => {
      if (this.debug) {
        alert("snapshot: " + JSON.stringify(snapshot));
        alert("minVersion = " + snapshot.val().minVersion);
      }
      minVersion = snapshot.val().minVersion;
      latestVersion = snapshot.val().latestVersion;
    });
    let versionNumber = await this.appVersion.getVersionNumber(); //.then(x =>{ versionNumber = x
    let comparison_min = this.compareVersion(versionNumber, minVersion);
    //let comparison_latest = this.compareVersion(versionNumber, latestVersion)
    
    if(comparison_min === "less"){
      this.disable_button = true;
      this.presentOldVersion(versionNumber,minVersion);
    }
  }

  presentConfirm(versionNumber, minVersion, latestVersion) {
    let comparison_min = this.compareVersion(versionNumber, minVersion);
    let comparison_latest = this.compareVersion(versionNumber, latestVersion)

    let messagetxt =
      "Your version of Salsa connect is behind... but not overly so.  Please update when you have a chance.<br><br> Your version: " +
      versionNumber +
        "<br> Minimum required: " +
        minVersion +
        "<br>Latest version: " + latestVersion;

    if (comparison_latest === "equal") {
      messagetxt =
        "Your version of Salsa connect is up to date!<br><br> Your version: " +
        versionNumber +
        "<br> latest version: " +
        latestVersion;
    }

    if (comparison_min === "less") {
      messagetxt =
        "YOUR VERSION OF SALSA CONNECT NEEDS TO BE UPDATED:<br><br> Your version: " +
        versionNumber +
        "<br> Minimum required: " +
        minVersion +
        "<br>Latest version: " + latestVersion;
    }
    let alertmsg = this.alertCtrl.create({
      title: name,
      message:
        messagetxt,
      buttons: [
        {
          text: "OK",
          role: "cancel",
          handler: () => {
            console.log("cancelled");
          }
        }
      ]
    });
    alertmsg.present();
  }

  compareVersion(a, b) {
    let a_arr = a.split(".", 3);
    let b_arr = b.split(".", 3);

    let a_int = a_arr.map(function(value) {
      let val = parseInt(value, 10);
      return val ? val : 0;
    });

    let b_int = b_arr.map(function(value) {
      let val = parseInt(value, 10);
      return val ? val : 0;
    });

    console.log(a_int);
    console.log(b_int);

    let status = "equal";
    let i = 0;

    do {
      if (a_int[i] < b_int[i]) status = "less";
      else if (a_int[i] > b_int[i]) status = "greater";
      i++;
    } while (status == "equal" && i < 3);

    return status;
  }

  presentOldVersion(versionNumber, minVersion) {

      let messagetxt =
        "YOUR VERSION OF SALSA CONNECT NEEDS TO BE UPDATED BEFORE YOU CAN CONTINUE:<br><br> Your version: " +
        versionNumber +
        "<br> Minimum required: " +
        minVersion
    
    let alertmsg = this.alertCtrl.create({
      title: "YOU MUST UPDATE",
      message:
        messagetxt,
      buttons: [
        {
          text: "OK",
          role: "cancel",
          handler: () => {
            console.log("cancelled");
          }
        }
      ]
    });
    alertmsg.present();
  }

  showFeedback(){
    this.navCtrl.push('FeedbackPage');
  }

  public openWithBrowserTab(url: string) {
        console.log("**********************************************")
        //console.log(isAvailable)
        console.log(url)
        console.log("**********************************************")

    this.browserTab.isAvailable()
      .then(isAvailable => {
        console.log("**********************************************")
        console.log(isAvailable)
        console.log(url)
        console.log("**********************************************")
        if (isAvailable) {
          this.browserTab.openUrl(url);
        } else {
          // open URL with InAppBrowser instead or SafariViewController
        }
      });
    }

}
