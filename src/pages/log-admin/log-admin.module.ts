import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LogAdminPage } from './log-admin';

@NgModule({
  declarations: [
    LogAdminPage,
  ],
  imports: [
    IonicPageModule.forChild(LogAdminPage),
  ],
})
export class LogAdminPageModule {}
