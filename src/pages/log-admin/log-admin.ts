import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from 'firebase';

/**
 * Generated class for the LogAdminPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-log-admin',
  templateUrl: 'log-admin.html',
})
export class LogAdminPage {
  private results = null;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams
  ) {
    firebase
        .database()
        .ref("/administration")
        .once("value")
        .then(snapshot => {
          //.orderByChild('displayName');
          var temp = snapshot.val();
          
          console.log(Object.keys(temp))
          
          var temp2 = Object.keys(temp).map(function(key) {
            return temp[key];
          });

          this.results = temp2.reverse();
          console.log(this.results);
        }); //end firebase.database
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad LogAdminPage');
  }

}
