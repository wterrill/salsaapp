import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountsEntryPage } from './accounts-entry';

@NgModule({
  declarations: [
    AccountsEntryPage,
  ],
  imports: [
    IonicPageModule.forChild(AccountsEntryPage),
  ],
})
export class AccountsEntryPageModule {}
