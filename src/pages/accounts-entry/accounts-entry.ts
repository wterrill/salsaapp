import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from "firebase";
import * as Fuse from "fuse.js";
import _ from "lodash";
import { AuthenticationProvider } from "../../providers/authentication/authentication";

@IonicPage()
@Component({
  selector: 'page-accounts-entry',
  templateUrl: 'accounts-entry.html',
})
export class AccountsEntryPage {

  private classArray = [];
  private classArray2 = [];

  private userArray = [];
  private userArray2 = [];

  private debug: boolean = true;
  private admin: boolean = false;
  private user = null;

  constructor(
    private navCtrl: NavController, 
    private navParams: NavParams,
    private auth: AuthenticationProvider) {
    if (this.auth.currentUserData.permissions === "administrator") {
      this.admin = true;
    }
    this.getClasses();
    this.user = this.auth.currentUser;
    //this.getUsers();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountsEntryPage');
  }

  ionViewWillEnter(){
    this.userArray = [];
    this.getUsers();
  }

  openClass(name,index){
    if(name === "Not yet Classified" ){
      name = "None"
    }
    let passed_list = null;
    console.log(name + " " + index)
    if(name == "All"){
      passed_list = this.userArray
    } else {
      passed_list = this.fuseSearch(this.userArray,name)
    }
    
    if (name === "Instructor"){
      name = "Instructors";
    }
    this.navCtrl.push("AccountsShowPage",{name: name, students: passed_list})
  }

  getClasses(){
    firebase
      .database()
      .ref("/availClasses/")
      //.orderByChild("birthday.month")
      //.orderByChild("displayName")
      .once("value")
      .then(snapshot => {
        var result = snapshot.val();
        for (var key in result) {
          if (result.hasOwnProperty(key)) {
            var val = result[key];
            this.classArray.push(val.structure);
          } //end of ir(result.hasOwnProperty(key))
        } //end var key in result
        
        // console.log(this.classArray);
        // console.log("here are the keys:"+Object.keys(result))
        // console.log("classArray:", this.classArray)

        function compare(a,b) {
          if (a.dayNum < b.dayNum)
            return -1;
          if (a.dayNum > b.dayNum)
            return 1;
          return 0;
        }
  
        this.classArray.sort(compare); //sort by day number.

        
        var index = this.classArray.map(e => e.shortName).indexOf('None');
        console.log("classArray:", this.classArray);
        this.classArray[index].shortName = "Not yet Classified";
        if(!this.admin){
          index = this.classArray.map(e => e.shortName).indexOf('Tester-only');
          this.classArray.splice(index,1)
        }


      }); //end firebase.database
  }

  getUsers(){
    firebase
      .database()
      .ref("/users/")
      //.orderByChild("birthday.month")
      .orderByChild("displayName")
      .once("value")
      .then(snapshot => {
        var result = snapshot.val();
        // console.log("result");
        // console.log(result);
        // console.log("snapshot");
        // console.log(snapshot);
        for (var key in result) {
          if (result.hasOwnProperty(key)) {
            var val = result[key];
            // console.log("key", key);
            // console.log("val");
            // console.log(val);
            // console.log(val.profile_pic);
            // console.log("___");
            val.uid = key;
            if (val.profile_pic != "undefined") {
              console.log(val);
              this.userArray.push(val);
            }
          } //end of ir(result.hasOwnProperty(key))
        } //end var key in result

        console.log(this.userArray);
        this.userArray2 = Object.keys(result).map(function (key) {
          return [Number(key), result[key]];
        });
        console.log("userArray:", this.userArray)
        console.log("userArray2:",this.userArray2);
        //this.setupDisplay(this.userArray);
        //this.filteredSearchItems = this.userArray;
      }); //end firebase.database
  }

  fuseSearch(searchTerm,searchArray) {
    //this does the actual fuzzy search.
    var options = {
      shouldSort: true,
      matchAllTokens: true,
      tokenize: true,
      includeScore: true,
      includeMatches: true,
      threshold: 0.1,
      //location: 0,  these values aren't needed if you match all tokens.
      //distance: 100,
      maxPatternLength: 32,
      minMatchCharLength: 1,
      findAllMatches: true,
      keys: ["firstName", "lastName", "currentClass.name", "displayName"]
    };

    if (this.debug) {
      console.log("fuzzywuzzy", searchTerm);
    }
    if(this.debug){
      console.log("searchArray:"+ searchArray)
      console.log(options)
      console.log(searchTerm)
    }
    var fuse = new Fuse(searchTerm, options); //this.result is the full-on data.
    var searchResults = fuse.search(searchArray);
    if (this.debug) {
      console.log("fuzzywuzzy-results", searchResults);
    }

    //Massaging the data before returning it. I'm taking the meta-data provided by
    //Fuse, and sticking it back into the filtered results.
    //the lodash map function below steps through each search result...
    var arr = _.map(searchResults, function (el) {
      el.item.matches = el.matches; // the matches matrix shows what actually matched during the search.  We're storing it inside of filteredSearchItems
      var matchedFoodArray = _.map(el.matches, function (le) {
        if (le.key == "menu_items.name") {
          return el.item.menu_items[le.arrayIndex];
        } //Here's we're using the array index for the matching items to look up the actual menu items.
      });
      if (matchedFoodArray[0] != undefined) {
        el.item.matching_food = matchedFoodArray; //the matching_food array is the one that will be displayed on the search screen results.
      }
      return el.item;
    });

    if (this.debug) {
      console.log("final array", arr);
    }
    return arr;
  } // end of fuse search

}
