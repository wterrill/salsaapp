import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TestFlextboxPage } from './test-flextbox';

@NgModule({
  declarations: [
    TestFlextboxPage,
  ],
  imports: [
    IonicPageModule.forChild(TestFlextboxPage),
  ],
})
export class TestFlextboxPageModule {}
