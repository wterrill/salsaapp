import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular'; //, NavParams


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }
  gotoNews(){
    this.navCtrl.push("NewsPage")
  }
  gotoLogLogin(){
    this.navCtrl.push("LogLoginPage");
  }
  gotoAdmin(){
    this.navCtrl.push("LogAdminPage");
  }
  gotoScratch(){
    this.navCtrl.push("ScratchPage")
  }
  gotoSwiper(){
    this.navCtrl.push("SwiperPage");
  }

  gotoVideo(){
    this.navCtrl.push("VideoPage");
  }

  gotoVideo2(){
    this.navCtrl.push("Video2Page");
  }

  gotoVideo3(){
    this.navCtrl.push("Video3Page");
  }

  gotoVideo4(){
    this.navCtrl.push("Video4Page");
  }

  gotoRoutine(){
    this.navCtrl.push("RoutinePage", { userParams: "1stRoutine" }, { animate: false });
  }

  gotoflex(){
    this.navCtrl.push("TestFlextboxPage");
  }

  gotologin(){
    this.navCtrl.push("LoginPage");
  }

  gotoSignUp(){
    this.navCtrl.push("SignUpPage");
  }

  gotopicker(){
    console.log('got here at least');
    this.navCtrl.push("VideoPickerPage");
  }

  gotoRestrictVideo(){
    this.navCtrl.push("RestrictVideoPage");
  }

  gotoRestrictBrowser(){
    this.navCtrl.push("FileBrowserPage");
  }
  
  gotoComponentTest(){
    this.navCtrl.push("ComponentTestPage");
  }

  gotoQR(){
    this.navCtrl.push("QrCodeStuffPage");
  }

  gotoSlide(){
    this.navCtrl.push("SlideshowPage");
  }

  gotoLanding(){
    this.navCtrl.push("LandingPage");
  }

  gotoQRS(){
    this.navCtrl.push("QrStudentPage");
  }

  gotoQRI(){
    this.navCtrl.push("QrInstructorPage");
  }

  gotoProfile(){
    this.navCtrl.push("CreateProfilePage");
  }
  
  gotoAccounts(){
    this.navCtrl.push("AccountsPage");
  }

  gotoLocalVideo(){
    this.navCtrl.push("LocalVideoPage");
  }

  gotoAccountsEntry(){
    this.navCtrl.push("AccountsEntryPage")
  }

  gotoUserManagement(){
    this.navCtrl.push("UserManagementPage")
  }
  

  
}
