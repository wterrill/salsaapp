import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SwiperPage } from './swiper';
import { SwipeCardsModule } from 'ng2-swipe-cards';

@NgModule({
  declarations: [
    SwiperPage,
  ],
  imports: [
    IonicPageModule.forChild(SwiperPage),
    SwipeCardsModule
  ],
})
export class SwiperPageModule {}
