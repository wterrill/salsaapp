import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { File } from '@ionic-native/file'
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer'; //FileUploadOptions
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the LocalVideoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-local-video',
  templateUrl: 'local-video.html',
})
export class LocalVideoPage {
  ////////////////////////////////////////////
  ///// G L O B A L    V A R I A B L E S////// 
  ////////////////////////////////////////////
  private debug: boolean = false;
  private path: string = null;
  private filename: string = null;
  private fileList: any;
  private showVid: boolean = false;
  private videoURL: string = "boobs";
  private server: string = "http://34.217.224.28/media/salsa/"
  ////////////////////////////////////////////

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public fileCtrl: File,
    public filexfer: FileTransfer,
    public alertCtrl: AlertController
    ) {
      //Initialize variables
      this.path = this.fileCtrl.dataDirectory + "files/";
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LocalVideoPage');
  }


//______________________________________________________________________________//
  downloadfile(){
      let url = this.server + this.filename;
        const fileTransfer: FileTransferObject = this.filexfer.create();
        fileTransfer.download(url, this.path + this.filename).then((entry) => {
         if(this.debug){
           alert('fileTransfer.download data:' + JSON.stringify(entry));
           }
         //this.getFileList();
       }, (err) => {
         // handle error
         alert("downloadfile() error: "+JSON.stringify(err));
       });
  }


//______________________________________________________________________________//
  getFileList()
 {
  this.fileCtrl.listDir(this.path, '').then(
    (files: any) => 
    {
      this.fileList = files;
      if(this.debug)
      {
        //alert("getFilesList");
      }
    }
  ).catch((err) => {if(this.debug){alert("catch for getFileList" + JSON.stringify(err))}});
  }


//______________________________________________________________________________//
  deletefile(file){
    this.fileCtrl.removeFile(this.path, file).then((result)=>
    {
      if(this.debug){
        alert("file deleted");
        alert(JSON.stringify(result))
      }
      //this.getFileList();
    })
   }


//______________________________________________________________________________//
   toggleVideo(){
     if(this.showVid === true){
       this.showVid = false
     } else {
       this.showVid = true;
     }
   }


//______________________________________________________________________________//
   loadVid(file){
     alert("loadVid")
     this.videoURL = this.path + file;
     this.videoURL = this.videoURL.replace(/file:\/\//g, "")
   }


//______________________________________________________________________________//
   selectAction(file) {
    let alertx = this.alertCtrl.create({
      title: 'Select Action:',
      message: `select the following action for this file:`,
      buttons: [
        {
          text: 'Delete',
          role: 'cancel',
          handler: () => {
            this.deletefile(file);
          }
        },
        {
          text: 'Load Vid',
          role: 'cancel',
          handler: () => {
            this.loadVid(file)
          }
        },        {
          text: 'File Size',
          role: 'cancel',
          handler: () => {
            let beer = this.getFileSize(this.path + file)

            beer.then(x=>{alert(x + " Bytes")});
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    alertx.present();
  }  


  getFileSize(fileUri) {
    return new Promise(function(resolve, reject) {    
        window['resolveLocalFileSystemURL'](fileUri, function(fileEntry) {
            fileEntry.file(function(fileObj) {
                resolve(fileObj.size);
            },
            function(err){
              alert("reject1")
            });
        }, 
        function(err){
            reject(err);
        });
    });
}


}
