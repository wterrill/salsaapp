import { Component } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular"; //NavParams
//import { Video4Page } from "../video4/video4";
import * as firebase from "firebase";
import { Storage } from "@ionic/storage";
import { File } from "@ionic-native/file";
import { LoadingController } from "ionic-angular";
//import * as _ from 'lodash';
import { AuthenticationProvider } from "../../providers/authentication/authentication";


/**
 * Generated class for the VideoPickerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//firebase.initializeApp();
//declare var cordova: any

@IonicPage()
@Component({
  selector: "page-video-picker",
  templateUrl: "video-picker.html"
})
export class VideoPickerPage {
  database: any;
  private videoPermissionArray: any;
  private debug: boolean = false;
  private fileList = [];
  private path: string;
  private loading = null;
  private uid = null;

  constructor(
    private navCtrl: NavController,
    //private navParams: NavParams,
    private storage: Storage,
    private filex: File,
    private loadingCtrl: LoadingController,
    private auth: AuthenticationProvider
  ) {
    this.loading = this.loadingCtrl.create({
      spinner: "ios",
      content: "Loading Routines..."
    });
    this.loading.present();

    this.path = this.filex.dataDirectory + "files/";
    this.database = firebase.database();
    this.storage.get("routineList").then(val => {
      console.log(val);
      this.updateVideoList(val);
      this.getFileList();
      this.loading.dismiss();
    });

    this.uid = this.auth.currentUser.uid;
    if(this.debug){alert("uid:" + this.uid);}
  }

  ionViewDidLoad() {
    // console.log("ionViewDidLoad VideoPickerPage");
    // this.storage.set("testing", "yup. tested");
  }

  ionViewDidEnter() {
    var videoArray = firebase.database().ref("users/" + this.uid + "/videoPermissions");
    videoArray.on("value", snapshot => {
      if(this.debug){alert("snapshot: " + JSON.stringify(snapshot))}
      this.storage.set("routineList", snapshot.val().array);
      this.updateVideoList(snapshot.val().array);
      if (this.debug) {
        alert("permissionlist " + this.videoPermissionArray);
      }
    });
  }

  updateVideoList(array) {
    this.videoPermissionArray = array;
  }

  goToRoutine(index) {
    console.log(index);
    this.navCtrl.push("Video4Page", { userParams: index }, { animate: false });
  }

  getFileList() {
    this.filex
      .listDir(this.path, "")
      .then((files: any) => {
        if (this.debug) {
          alert(JSON.stringify(files[0]));
          alert(JSON.stringify(files[0].nativeURL));
          alert(JSON.stringify(this.filex.dataDirectory));
          alert(this.path);
          alert(JSON.stringify(files));
          alert(files[0].name);
        }
        this.fileList = files.map(a => a.name);
        
        this.compareAndDelete();
      })
      .catch(err => {
        //alert("getFileList " + JSON.stringify(err));
        console.log("getFileList", JSON.stringify(err)) 
      });
  }

  compareAndDelete() {
    // TODO this should only check file names before the "-" in 1stRoutine-oldCam_1.mp4  
    this.fileList.forEach(a => {
      let temp = a.replace(/\..*/g, "");
      let temp2 = temp.split("-")
      temp = temp2[0]
      //   s = s.substring(0, s.indexOf('?'))      or     url.split('?')[0]
      if (this.debug) {
        alert(a);
        alert(temp);
        alert(this.videoPermissionArray);
      }
      if (this.videoPermissionArray.find(k => k == temp) != null) {
        if (this.debug) {
          alert("don't delete " + a);
        }
      } else {
        if (this.debug) {
          alert("delete " + a);
        }
        this.deletefile(a);
      }
    });
  }

  deletefile(file) {
    // alert(file);
    // alert(this.path)
    this.filex.removeFile(this.path, file).then(result => {
      if (this.debug) {
        alert("file deleted: " + file);
      }
      if (this.debug) {
        alert(JSON.stringify(result));
      }
      this.getFileList();
    });
  }


  
}
