import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular'; //NavParams
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import * as firebase from "firebase";
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {
  private todo: FormGroup;
  private email_var: string = null;

  constructor(
    public navCtrl: NavController,
    private formBuilder: FormBuilder,
    public alertCtrl: AlertController     
  ) {
    this.todo = this.formBuilder.group({
      email_form: ["", Validators.compose([Validators.required, Validators.email, Validators.pattern("^(?=.*\.)(?=.*@).+")  ])],
    });
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad ForgotPasswordPage");
  }



  submitted(todoValues) {  
    let that = this;
    firebase.auth().sendPasswordResetEmail(this.email_var).then(function() {
      that.presentInfo();
    }).catch(function(error) {
      alert(error)
    });

    
  }



  

  presentInfo() {
    let alert = this.alertCtrl.create({
      title: 'Reset Email',
      message: `A reset Email has been sent to your email address.<br> 
                Please follow the instructions in the link to reset the email.`,
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          handler: () => {
            this.navCtrl.setRoot("LoginPage");
          }
        }
      ]
    });
    alert.present();
  }  
}





