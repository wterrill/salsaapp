import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountsShowPage } from './accounts-show';


@NgModule({
  declarations: [
    AccountsShowPage
  ],
  imports: [
    IonicPageModule.forChild(AccountsShowPage)
  ],
})
export class AccountsShowPageModule {}
