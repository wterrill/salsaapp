import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';  //Popover
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MyApp } from './app.component';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { VideoPlayer } from '@ionic-native/video-player';
import * as firebase from 'firebase';
import { File } from '@ionic-native/file'
import { FileTransfer } from '@ionic-native/file-transfer';
import { IonicStorageModule } from '@ionic/storage';
import { BarcodeScanner } from '@ionic-native/barcode-scanner'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { Camera } from '@ionic-native/camera';  //CameraOptions
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { AppVersion } from '@ionic-native/app-version';
import { EmailComposer } from '@ionic-native/email-composer';

import { PopoverComponent} from '../components/popover/popover';
import { SwipeCardsModule } from 'ng2-swipe-cards';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { BrowserTab } from '@ionic-native/browser-tab';


//for lazy loading
import { HttpModule } from '@angular/http';
import { ConfigurationProvider } from '../providers/configuration/configuration';


  // Initialize Firebase
  export const config = {
    apiKey: "AIzaSyCuZR55hTPEPP5OfTUT4EEy8GL5Wd8MX5E",
    authDomain: "salsaapp-3649f.firebaseapp.com",
    databaseURL: "https://salsaapp-3649f.firebaseio.com",
    projectId: "salsaapp-3649f",
    storageBucket: "salsaapp-3649f.appspot.com",
    messagingSenderId: "1089649434600"
  };

  firebase.initializeApp(config);
  

@NgModule({
  declarations: [
    MyApp,
    PopoverComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    BrowserAnimationsModule,
    SwipeCardsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PopoverComponent,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    YoutubeVideoPlayer,
    VideoPlayer,
    File,
    FileTransfer,
    BarcodeScanner,
    AuthenticationProvider,
    Camera,
    ScreenOrientation,
    AppVersion,
    EmailComposer,
    InAppBrowser,
    BrowserTab,
    ConfigurationProvider
  ]
})
export class AppModule {}

